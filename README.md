# Tutorial Blockchain

## Introdução

Uma Blockchain é, resumidamente, uma série datada de registro de dados imutáveis e é gerenciada por clusters de computadores e não é possuída por nenhum entidade única. Cada um dos blocos da Blockchain são seguros e ligados uns aos outros usando principios de criptografia, no nosso caso, com o algoritmo SHA256.

Por não ter uma autoridade central, a Blockchain é a definição literal de sistema democratizado. Uma vez que tem essa natureza distribuída e imutável, a informação é aberta para qualquer um verificar. Portanto qualquer coisa construída com blockchain é, por natureza própria, transparente e todo mundo envolvido é responsável por suas ações.

## Explicação

A Blockchain é uma forma simples mas engenhosa de passar uma determinada informação de A para B em uma maneira automatizada e segura. 

A transação é iniciada quando um bloco é criado por alguém da rede. O bloco então é verificado por milhares, talvez até milhões de computadores distribuídos pela rede. 

O bloco após ser verificado é adicionado a corrente, que é armazenada pela rede, criando não somente um registro único, mas também uma histórico único. Falsificar um registro, significaria falsificar a corrente inteira em milhões de instâncias.

## Cases

O case mais famoso de blockchain é certamente nas criptomoedas como o Bitcoin. Por ter a arquitetura descentralizada e com o número gigantesco de computadores que compõe a rede do Bitcoin, tudo isso contribui para a robustez do sistema como um todo, no sentido de evitar fraudes ou indisponibilidades.

Um case um pouco menos famoso, mas tão interessante quanto o das criptomoedas, é o das cadeias de suprimento. O mercado de produção de café é notoriamente complexo e possui inúmeros intermediários, cada um tirando sua parte no decorrer do trajeto. Os produtores propriamente ditos, ficam sempre com a menor parte: geralmente 2% do valor agregado de cada xícara de café chega aos bolsos dos produtores.

A plataforma bext360 provê a todos os stakeholders (produtores, torradores de café e consumidores), acesso aos dados de toda a cadeia de suprimentos. Esses dados possibilitam um nível completamente novo de transparência no que se refere a origem e qualidade.

A plataforma funciona com Cryptotokens: no ponto de coleta do café, a plataforma cria criptotokens que representa o valor da commodity. De acordo a commodity passa através da cadeira de suprimentos, novas tokens são criadas. Essas tokens aumentam de valor de acordo elas se movem pela cadeia. Esse processo de passo a passo faz com que a cadeia seja interamente transparente e faz a distribuição de valres mais fácil.

## Código

Em suma, uma blockchain deve ser formada por blocos que tem vários metadados como um ponteiro para o próximo bloco, o hash do bloco anterior, uma timestamp, o dado que é guardado pela blockchain em si (como dados de transações ou etapas da cadeia de suprimentos) e seu próprio hash que é calculado com todos esses valores.

Para vericar se uma blockchain está toda válida, basta checar se os hashes de cada bloco batem com o hash de seu bloco subjacente.

```python
import datetime
import hashlib
import copy

#Classe bloco que compõe a blockchain
class Block:
    numBlock = 0    #número do bloco
    dado = None     #o dado a ser armazenado
    prox = None     #ponteiro para o próximo bloco
    hash_anterior = 0x0 #hash do bloco anterior
    timestamp = datetime.datetime.now() #data e hora de criação do bloco

    #construtor
    def __init__(self, dado):
        self.dado = dado

    #criação do hash utilizando todos os dados que compõe o bloco
    def hash(self):
        h = hashlib.sha256()
        h.update(
        str(self.dado).encode('utf-8') +
        str(self.hash_anterior).encode('utf-8') +
        str(self.timestamp).encode('utf-8') +
        str(self.numBlock).encode('utf-8')
        )
        return h.hexdigest()

    #string com os valores do bloco
    def __str__(self):
        return "Hash: " + str(self.hash()) + "\nHash anterior: " + str(self.hash_anterior) + "\nNúmero: " + str(self.numBlock) + "\nDado: " + str(self.dado) + "\n--------------"

class Blockchain:

    #primeiro bloco de uma blockchain é chamado de bloco genesis e geralmente é hard coded
    block = Block("Genesis")

    #inicializa a variável head com referência ao bloco genesis
    head = block
    
    #inicializa a variável primeiro com o valor da variável head
    primeiro = copy.deepcopy(head)

    #insere um bloco 
    def insere(self, block):
        #atribui o valor de hash_anterior como o hash do bloco que é atualmente o head da blockchain
        block.hash_anterior = self.block.hash()
        #seta o número do bloco
        block.numBlock = self.block.numBlock + 1
        #seta o ponteiro de proximo do head para o novo bloco
        self.block.prox = block
        #seta o head como o novo bloco
        self.block = self.block.prox

    def resetHead(self):
        #reseta o valor da head para o bloco genesis
        self.head = copy.deepcopy(primeiro)

    #checa se todos os hashes batem com o hash de seus blocos subjacentes
    def validaBlockChain(self):
        self.resetHead()
        while self.head != None:
            if self.head.hash != self.head.prox.hash_anterior:
                return False
        return True


blockchain = Blockchain()

#cria 10 blocos com o dado Block N, onde n varia de 0 a 10
for n in range(10):
    blockchain.insere(Block("Block " + str(n+1)))

#imprime os blocos
while blockchain.head != None:
    print(blockchain.head)
    blockchain.head = blockchain.head.prox

#verifica se a blockchain é valida
if blockchain.validaBlockChain:
    print('Blockchain válido!')

```

Baseado nestes códigos-fonte:

https://github.com/ivan-liljeqvist/SimpleBlockchain

https://github.com/howCodeORG/Simple-Python-Blockchain/blob/master/blockchain.py