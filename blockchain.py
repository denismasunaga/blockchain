import datetime
import hashlib
import copy

class Block:
    numBlock = 0
    dado = None
    prox = None
    hash_anterior = 0x0
    timestamp = datetime.datetime.now()

    def __init__(self, dado):
        self.dado = dado

    def hash(self):
        h = hashlib.sha256()
        h.update(
        str(self.dado).encode('utf-8') +
        str(self.hash_anterior).encode('utf-8') +
        str(self.timestamp).encode('utf-8') +
        str(self.numBlock).encode('utf-8')
        )
        return h.hexdigest()

    def __str__(self):
        return "Hash: " + str(self.hash()) + "\nHash anterior: " + str(self.hash_anterior) + "\nNúmero: " + str(self.numBlock) + "\nDado: " + str(self.dado) + "\n--------------"

class Blockchain:

    block = Block("Genesis")
    head = block
    primeiro = copy.deepcopy(head)

    def insere(self, block):
        block.hash_anterior = self.block.hash()
        block.numBlock = self.block.numBlock + 1
        self.block.prox = block
        self.block = self.block.prox

    def resetHead(self):
        self.head = copy.deepcopy(primeiro)

    def validaBlockChain(self):
        self.resetHead()
        while self.head != None:
            if self.head.hash != self.head.prox.hash_anterior:
                return False
        return True


blockchain = Blockchain()

for n in range(10):
    blockchain.insere(Block("Block " + str(n+1)))

while blockchain.head != None:
    print(blockchain.head)
    blockchain.head = blockchain.head.prox

if blockchain.validaBlockChain:
    print('Blockchain válido!')